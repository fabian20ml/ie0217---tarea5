#include "prioridad.hpp"
using namespace std;

int main(int argc, char const *argv[])
{   
    if (argc != 3)
    {
        cout << "Error en el numero de parametros." << endl;
    } else {
        int numColas = atoi(argv[1]);
        string priority(argv[2]);
        int argumentos = priority.size();
        if ((numColas+numColas-1) != argumentos) 
        {
            cout << "La cantidad de colas y los parametros de pioridad no coinciden." << endl;
        } else {
            int prioridad[numColas];
            for (int i = 0; i < numColas; i++)
            {
                prioridad[i] = priority[2*i]-'0';
            }
            SistemaPrioridadColas(numColas,prioridad);
            cout << endl;
        }
    }

	return 0;

}