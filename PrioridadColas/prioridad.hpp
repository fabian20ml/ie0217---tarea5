#include <iostream>
#include "Element.h"
#include <string>
#include "Queue.h"
#include <random>

using namespace std;
#define Data Element<int>

void SistemaPrioridadColas(int n, int prioridad[]){
	
	Queue<Data>* colas[n];
	bool notEmpty[n];
	std::random_device rd;     
	std::mt19937 rng(rd()); 
	std::uniform_int_distribution<int> aleatorio(0,10); 
	std::uniform_int_distribution<int> aleatorio2(1,10); 
	// Llenar colas
	for (int i = 0; i < n; i++)
	{
		colas[i] = new Queue<Data>();
		notEmpty[i] = true;
		cout << "cola" << i << ": ";
		int numOfElements = aleatorio2(rng);
		for (int j = 0; j < numOfElements; j++)
		{
			int randomNum = aleatorio(rng);
			cout << randomNum << ", ";
			colas[i]->enqueue(Data(randomNum));
		}
		cout << endl;
	}
	bool keepGoing = true;
	cout << "Salida: ";
	while (keepGoing)
	{	
		// Por cada cola
		for (int i = 0; i < n; i++)
		{
			if (notEmpty[i])
			{
				// por respectiva prioridad
				for (int j = 0; j < prioridad[i]; j++)
				{
					Data numero = colas[i]->dequeue();
					if (numero.isValid())
					{
						cout  << numero.get() << " ,";
					} else {
						notEmpty[i] = false;
						j = prioridad[i];
					}
					
				}
			}
		}
		
		// Condicion para parar (Se vaciaron las colas)
		bool stop = false;
		for (int i = 0; i < n; i++)
		{
			stop = stop|notEmpty[i];
		}
		keepGoing = stop;
	}
	
	for (int i = 0; i < n; i++)
	{
		delete(colas[i]);
	}
	
	
}