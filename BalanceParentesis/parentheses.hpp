#include <iostream>
#include "Element.h"
#include <string>
#include "Stack.h"

using namespace std;
#define Data Element<char>

bool checkBalance(string hilera){
	bool valid = true;
	Stack<Data> checker;
	int sizeHilera = hilera.length();
	int ElementsPopped = 0;
	int ElementsPushed = 0;
	for (int i = 0; i < sizeHilera; i++)
	{
		Data top;
		if (hilera.at(i) == '{' || hilera.at(i) == '[' || hilera.at(i) == '(')
		{
			checker.push(Data(hilera.at(i)));
			ElementsPushed++;
		} else if (hilera.at(i) == '}')
		{
			top = checker.pop();
			ElementsPopped++;
			if (top.get() != '{')
			{
				valid = false;
			}
		} else if (hilera.at(i) == ']')
		{
			top = checker.pop();
			ElementsPopped++;
			if (top.get() != '[')
			{
				valid = false;
			}
		} else if (hilera.at(i) == ')')
		{
			top = checker.pop();
			ElementsPopped++;
			if (top.get() != '(')
			{
				valid = false;
			}
		}
	}
	
	if (ElementsPopped!=ElementsPushed)
	{
		valid = false;
	}
	
	return valid;
}